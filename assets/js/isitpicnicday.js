/**
 * isitpicnicday.js?
 */
(function (window, moment) {
  'use strict'

  /*************
   * CONSTANTS *
   *************/

  var PD_SEASON_CUTOFF = 21 // 3 weeks

  /************
   * UTILTIES *
   ************/

  function loadTemplate (templateId, wrapperId) {
    if ('content' in document.createElement('template')) {
      var messageWrapper = document.querySelector('#' + wrapperId)
      var template = document.querySelector('#' + templateId)
      var clone = document.importNode(template.content, true)
      messageWrapper.appendChild(clone)
    } else {
      throw new Error('HTML <template> not supported!')
    }
  }

  function clearChildren (nodeId) {
    var node = document.querySelector(nodeId)
    while (node.firstChild) {
      node.removeChild(node.firstChild)
    }
  }

  function showNegMessageAndTimer () {
    clearChildren('#pd-message')
    loadTemplate('pd-neg-template', 'pd-message')
    document.querySelector('#watermark').classList.remove('hype-bg')
  }

  function showPosMessage () {
    clearChildren('#pd-message')
    loadTemplate('pd-pos-template', 'pd-message')
    document.querySelector('#watermark').classList.add('hype-bg')
  }

  // FIXME: dynamically set title of site based on status
  // function setTitle (title) { document.title = title }

  /**
   * Pads a number with a characters
   *
   * @param {number} num - The input number to display
   * @param {number} width - The max characters the output should be
   * @param {string} char - The character to pad the input number with (to the left)
   * @returns {string}
   */
  function pad (num, width, char) {
    var padChar = char || '0'
    var numString = num.toString()

    if (numString.length < width) {
      return new Array(width - numString.length + 1).join(padChar) + numString
    }

    return numString
  }

  /**
   * Pluralizes a string
   *
   * @param {string} string - The unit name
   * @param {number} num - Number of the unit to determine plurality
   * @returns
   */
  function _pluralize (string, num) {
    return string + (num !== 1 ? 's' : '')
  }

  /**
   * Calculates the opacity number to set the background at, based on how much time is between now and the event date
   */
  function calcPartialOpacity (timeLeftInMs) {
    var daysUntil = Math.floor(timeLeftInMs / (1000 * 60 * 60 * 24))

    if (daysUntil === 0) {
      return 1
    } else if (daysUntil < PD_SEASON_CUTOFF && daysUntil !== 0) {
      return 0.5 * ((PD_SEASON_CUTOFF - daysUntil + 1) / PD_SEASON_CUTOFF)
    }

    return 0
  }

  /**
   * @class
   * Manages picnic day dates and comparing dates
   */
  function DateManager (dates) {
    this.eventDates = dates || []
  }

  DateManager.prototype.getUpcoming = function () {
    for (var i = 0, len = this.eventDates.length; i < len; i++) {
      var date = this.eventDates[i]
      if (new Date() < date) return date
    }
    return null
  }

  DateManager.prototype.isEventToday = function () {
    var today = moment()
    for (var i = 0, len = this.eventDates.length; i < len; i++) {
      var date = this.eventDates[i]
      var eventIsToday = today.isBetween(date, moment(date).add(1, 'd'), null, '[)')
      return eventIsToday
    }
  }

  /**
   * @class
   *
   * Manages the countdown timer
   */
  function TimeDisplay (countdownInMs) {
    this.countdownTime = countdownInMs
    this.counterClass = 'timer-counter'
    this.numberClass = 'timer-number'
    this.unitClass = 'timer-unit'
  }

  TimeDisplay.prototype.tick = function () {
    this.countdownTime -= 1000

    var timeArray = this._getTimeArray()

    this.setSeconds(timeArray[3])
    this.setMinutes(timeArray[2])
    this.setHours(timeArray[1])
    this.setDays(timeArray[0])
  }

  TimeDisplay.prototype._setTime = function (wrapperEl, num, unit) {
    if (!wrapperEl) {
      console.error('no wrapper element found', wrapperEl)
      return
    }

    if (num > -1) {
      wrapperEl.querySelector('.' + this.numberClass).textContent = pad(num, 2)
    }

    if (unit) {
      wrapperEl.querySelector('.' + this.unitClass).textContent = unit
    }
  }

  /**
   * Breaks the countdown (in seconds) into an array of units until of time left
   *
   * @param {number} time - Countdown to event in seconds
   * @returns {Array} In Day, Hour, Minute, Second format
   */
  TimeDisplay.prototype._getTimeArray = function () {
    var days = this.countdownTime / (1000 * 60 * 60 * 24)
    var hours = (days % 1) * 24
    var minutes = (hours % 1) * 60
    var seconds = (minutes % 1) * 60

    return [days, hours, minutes, seconds].map(Math.floor)
  }

  TimeDisplay.prototype.setSeconds = function (timeNum) {
    var el = document.querySelector('.' + this.counterClass + '.seconds')
    this._setTime(el, timeNum, _pluralize('second', timeNum))
  }

  TimeDisplay.prototype.setMinutes = function (timeNum) {
    var el = document.querySelector('.' + this.counterClass + '.minutes')
    this._setTime(el, timeNum, _pluralize('minute', timeNum))
  }

  TimeDisplay.prototype.setHours = function (timeNum) {
    var el = document.querySelector('.' + this.counterClass + '.hours')
    this._setTime(el, timeNum, _pluralize('hour', timeNum))
  }

  TimeDisplay.prototype.setDays = function (timeNum) {
    var el = document.querySelector('.' + this.counterClass + '.days')
    this._setTime(el, timeNum, _pluralize('day', timeNum))
  }

  /*************************
   * LISTENERS AND ACTIONS *
   *************************/
  window.playSong = function () {
    var audioEl = document.querySelector('#pd-song')
    audioEl.play()
    document.querySelectorAll('.btn').forEach(function (el) {
      el.classList.toggle('btn--hidden')
    })
  }

  window.pauseSong = function () {
    var audioEl = document.querySelector('#pd-song')
    audioEl.pause()
    document.querySelectorAll('.btn').forEach(function (el) {
      el.classList.toggle('btn--hidden')
    })
  }

  /************
   * DO STUFF *
   ************/
  var picnicDayManager = new DateManager([
    new Date(2021, 3, 17),
    new Date(2022, 3, 16)
  ])

  var queryParams = new URLSearchParams(window.location.search)
  var flags = {
    FORCE_POSITIVE: false
  }

  // Artificially set status as event date
  if (queryParams.has('__itspicnicday') && queryParams.get('__itspicnicday') === '1') {
    flags.FORCE_POSITIVE = true
  }

  // Artificially set countdown time by setting fake date (x seconds) in the future
  if (queryParams.has('__setCountdown')) {
    picnicDayManager = new DateManager([
      moment().add(queryParams.get('__setCountdown'), 's').toDate()
    ])
  }

  var clock = null
  var watermarkEl = document.querySelector('#watermark')
  var watermarkOpacity = watermarkEl.style.opacity

  // Main Loop, ticks every second
  setInterval(function () {
    var isToday = picnicDayManager.isEventToday() || flags.FORCE_POSITIVE

    if (isToday) {
      if (!document.querySelector('#pd-pos')) {
        showPosMessage()
      }

      watermarkOpacity = '1'
    } else {
      if (!document.querySelector('#pd-neg')) {
        showNegMessageAndTimer()

        var timeLeft = moment(picnicDayManager.getUpcoming()).diff(new Date(), 'milliseconds')
        clock = new TimeDisplay(timeLeft)
      }

      if (!clock) {
        console.error('clock has not been initialized!')
      } else {
        clock.tick()
      }

      // Set BG opacity of watermark
      watermarkOpacity = calcPartialOpacity(clock.countdownTime).toString()
    }

    watermarkEl.style.opacity = watermarkOpacity
  }, 1000)
})(window, window.moment)
